import java.util.Scanner;

public class numero_harshad {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingrese un número: ");
        int num = sc.nextInt();
        
        int sum = 0;
        int n = num;
        
        while (n != 0) {
            int digit = n % 10;
            sum += digit;
            n /= 10;
        }
        
        if (num % sum == 0) {
            System.out.println(num + " es un número de Harshad.");
        } else {
            System.out.println(num + " no es un número de Harshad.");
        }
    }
}
