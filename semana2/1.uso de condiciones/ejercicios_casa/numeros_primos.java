import java.util.Scanner;

public class numeros_primos {
    
    public static boolean esPrimo(int num) {
        if (num < 2) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingrese la cantidad de números primos que desea mostrar: ");
        int n = sc.nextInt();
        
        int count = 0;
        int num = 2;
        
        while (count < n) {
            if (esPrimo(num)) {
                System.out.println(num);
                count++;
            }
            num++;
        }
    }
}
