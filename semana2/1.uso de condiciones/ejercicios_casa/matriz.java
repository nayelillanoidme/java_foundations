import java.util.Scanner;

public class matriz {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingrese la cantidad de números a ordenar: ");
        int n = sc.nextInt();
        
        int[] nums = new int[n];
        
        System.out.println("Ingrese los números a ordenar: ");
        for (int i = 0; i < n; i++) {
            nums[i] = sc.nextInt();
        }
        
        System.out.println("Matriz antes de ordenar: " + Arrays.toString(nums));
        
        // Método de inserción
        for (int i = 1; i < n; i++) {
            int key = nums[i];
            int j = i - 1;
            
            while (j >= 0 && nums[j] > key) {
                nums[j + 1] = nums[j];
                j--;
            }
            
            nums[j + 1] = key;
        }
        
        System.out.println("Matriz después de ordenar: " + Arrays.toString(nums));
    }
}
