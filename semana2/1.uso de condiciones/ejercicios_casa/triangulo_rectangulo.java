import java.util.Scanner;

public class triangulo_rectangulo {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingrese un número entero: ");
        int numero = sc.nextInt();
        
        for (int i = 1; i <= numero; i += 2) {
            for (int j = i; j >= 1; j -= 2) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}

