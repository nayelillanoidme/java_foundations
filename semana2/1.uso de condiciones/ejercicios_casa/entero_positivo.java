import java.util.Scanner;

public class entero_positivo{
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingrese un número entero positivo: ");
        int numero = sc.nextInt();
        
        for (int i = 1; i <= numero; i += 2) {
            System.out.print(i + "; ");
        }
    }
}
