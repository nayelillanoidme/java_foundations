public class factorial_numerico {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Debe ingresar un valor numérico como argumento.");
        } else {
            try {
                int num = Integer.parseInt(args[0]);
                long factorial = 1;
                for (int i = 2; i <= num; i++) {
                    factorial *= i;
                }
                System.out.println("El factorial de " + num + " es " + factorial);
            } catch (NumberFormatException e) {
                System.out.println("El valor ingresado no es un número entero válido.");
            }
        }
    }
}

