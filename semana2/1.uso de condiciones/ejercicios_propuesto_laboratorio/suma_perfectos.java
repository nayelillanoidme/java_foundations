
import java.util.ArrayList;
import java.util.Scanner;

public class suma_perfectos{
    public static boolean esPerfecto(int n) {
        int suma = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                suma += i;
            }
        }
        return suma == n;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el valor de N: ");
        int n = sc.nextInt();
        ArrayList<Integer> numPerfectos = new ArrayList<Integer>();
        int i = 1;
        while (numPerfectos.size() < n) {
            if (esPerfecto(i)) {
                numPerfectos.add(i);
            }
            i++;
        }
        int suma = 0;
        for (int num : numPerfectos) {
            suma += num;
        }
        System.out.println("La suma de los primeros " + n + " números perfectos es: " + suma);
    }
}
    

