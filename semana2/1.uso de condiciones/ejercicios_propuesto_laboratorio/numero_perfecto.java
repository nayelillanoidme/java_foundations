import java.util.Scanner;

public class numero_perfecto {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduce un número: ");
        int numero = scanner.nextInt();
        int suma = 0;
        int i = 1;
        
        do {
            if (numero % i == 0) {
                suma += i;
            }
            i++;
        } while (i < numero);
        do {
            if (numero % i == 0) {
                suma += i;
            }
            i++;
        } while (i < numero);
        
        if (suma == numero) {
            System.out.println(numero + " es un número perfecto.");
        } else {
            System.out.println(numero + " no es un número perfecto.");
        }
    }
}
