public class Main {
    public static void main(String[] args) {
        int[] asciiArray = new int[256];

        for (int i = 0; i < 256; i++) {
            asciiArray[i] = i;
        }

        System.out.println("Arreglo de códigos ASCII:");
        for (int i = 0; i < 256; i++) {
            System.out.print(asciiArray[i] + " ");
        }
    }
}
