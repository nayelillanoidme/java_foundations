import java.util.*;

public class enteros_mediana {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el tamaño del arreglo: ");
        int n = scanner.nextInt();

        int[] arr = new int[n];
        System.out.println("Ingrese los elementos del arreglo:");

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        double mean = findMean(arr);

        System.out.println("La media del arreglo es: " + mean);
    }

    public static double findMean(int[] arr) {
        int sum = 0;

        for (int num : arr) {
            sum += num;
        }

        return (double) sum / arr.length;
    }
}
