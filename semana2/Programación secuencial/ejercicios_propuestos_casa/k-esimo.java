int[] arr = {3, 5, 2, 8, 4, 10};
int k = 3;

int kthLargest = findKthLargest(arr, k);

System.out.println("El " + k + "-ésimo elemento más grande es: " + kthLargest);

public static int findKthLargest(int[] nums, int k) {
    int n = nums.length;
    int target = n - k;

    int left = 0, right = n - 1;

    while (left <= right) {
        int pivot = nums[(left + right) / 2];

        int i = left;
        for (int j = left; j < right; j++) {
            if (nums[j] <= pivot) {
                swap(nums, i, j);
                i++;
            }
        }
        swap(nums, i, right);
        if (i == target) {
            return nums[i];
        } else if (i < target) {
            left = i + 1;
        } else {
            right = i - 1;
        }
    }

    return -1; 
}

private static void swap(int[] nums, int i, int j) {
    int temp = nums[i];
    nums[i] = nums[j];
    nums[j] = temp;
}
