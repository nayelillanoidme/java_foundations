public class numeros_impares {
    public static void main(String[] args) {
        int[] arr = {4, 8, 2, 1, 9, 6};
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                count++;
            }
        }

        System.out.println("Hay " + count + " números impares en el arreglo.");
    }
}
