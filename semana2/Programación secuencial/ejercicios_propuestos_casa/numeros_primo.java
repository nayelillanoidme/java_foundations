int[] arr = {2, 3, 5, 7, 8, 10};
int count = countPairsWithPrimeSum(arr);

System.out.println("El número de parejas de elementos cuya suma es un número primo es: " + count);

public static int countPairsWithPrimeSum(int[] nums) {
    int n = nums.length;
    int count = 0;

    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            if (isPrime(nums[i] + nums[j])) {
                count++;
            }
        }
    }

    return count;
}

public static boolean isPrime(int n) {
    if (n <= 1) {
        return false;
    }

    for (int i = 2; i <= Math.sqrt(n); i++) {
        if (n % i == 0) {
            return false;
        }
    }

    return true;
}
