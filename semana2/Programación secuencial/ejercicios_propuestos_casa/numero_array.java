import java.util.*;

public class numero_array{
    public static void main(String[] args) {
        int[] arr = {2, 3, 2, 5, 6, 7, 5, 3, 2, 1, 1, 7, 7, 7};
        int maxCount = 0;
        int maxNumber = 0;
        
        Map<Integer, Integer> frequencyMap = new HashMap<>();
        
        for (int num : arr) {
            int count = frequencyMap.getOrDefault(num, 0) + 1;
            frequencyMap.put(num, count);
            if (count > maxCount) {
                maxCount = count;
                maxNumber = num;
            }
        }
        
        System.out.println("El número que se repite más veces en el array es: " + maxNumber);
    }
}
