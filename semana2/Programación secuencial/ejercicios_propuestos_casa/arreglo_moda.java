import java.util.*;

public class arreglo_moda{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el tamaño del arreglo: ");
        int n = scanner.nextInt();

        int[] arr = new int[n];
        System.out.println("Ingrese los elementos del arreglo:");

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        int mode = findMode(arr);

        System.out.println("La moda del arreglo es: " + mode);
    }

    public static int findMode(int[] arr) {
        Map<Integer, Integer> freq = new HashMap<>();
        int maxFreq = 0;
        int mode = 0;

        for (int num : arr) {
            freq.put(num, freq.getOrDefault(num, 0) + 1);
            if (freq.get(num) > maxFreq) {
                maxFreq = freq.get(num);
                mode = num;
            }
        }

        return mode;
    }
}
