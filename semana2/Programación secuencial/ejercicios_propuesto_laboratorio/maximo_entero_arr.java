import java.util.Scanner;

public class maximo_entero_arr {

    int numero_mayor(int n){
        return 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa el tamaño del arreglo: ");
        int n = scanner.nextInt();
        int[] arreglo = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Ingresa el elemento " + (i + 1) + ": ");
            arreglo[i] = scanner.nextInt();
        }
        int maximo = maximo(arreglo);
        System.out.println("El máximo es: " + maximo);
        scanner.close();
    }

    public static int maximo(int[] arreglo) {
        int maximo = arreglo[0];
        for (int i = 1; i < arreglo.length; i++) {
            if (arreglo[i] > maximo) {
                maximo = arreglo[i];
            }
        }
        return maximo;
    }
}
