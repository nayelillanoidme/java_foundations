import java.util.Scanner;

public class llover {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("¿Está lloviendo? ");
        String lluvia = input.nextLine().toLowerCase();

        if (lluvia.equals("sí")) {
            System.out.print("¿Está haciendo mucho viento? ");
            String viento = input.nextLine().toLowerCase();
            if (viento.equals("sí")) {
                System.out.println("Hace mucho viento para salir con una sombrilla.");
            } else {
                System.out.println("Lleva una sombrilla por si acaso.");
            }
        } else {
            System.out.println("Que tengas un bonito día.");
        }
    }
}
