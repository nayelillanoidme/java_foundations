import java.util.Scanner;

public class estudiantesenati {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de estudiantes: ");
        int n = input.nextInt();

        String[][] estudiantes = new String[n][2];
        for(int i = 0; i < n; i++) {
            System.out.print("Ingrese el nombre del estudiante " + (i+1) + ": ");
            estudiantes[i][0] = input.next();
            System.out.print("Ingrese la edad del estudiante " + (i+1) + ": ");
            estudiantes[i][1] = input.next();
        }

        double edadtotal = sumAges(estudiantes, n);
        double averageAge = calculateAverage(edadtotal, n);
        System.out.println("La edad promedio de los estudiantes es: " + averageAge);
    }

    public static double sumAges(String[][] students, int n) {
        double edadtotal = 0;
        for(int i = 0; i < n; i++) {
            edadtotal += Double.parseDouble(students[i][1]);
        }
        return edadtotal;
    }

    public static double calculateAverage(double totalAge, int n) {
        return totalAge / n;
    }
}