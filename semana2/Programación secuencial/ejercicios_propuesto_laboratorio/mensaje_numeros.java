import java.util.Scanner;

public class mensaje_numeros  {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero;
        String mensaje;

        do {
            System.out.print("Ingrese un número del 1 al 7: ");
            numero = sc.nextInt();
        } while (numero < 1 || numero > 7);

        switch (numero) {
         case 1:
                mensaje = "hoy aprendemos sobre pogramacion";
                break;
         case    2:
                mensaje = "que tal tomar un curso de marketing";
                break;
         case    3:
                mensaje = "hoy es un gran dia para aprender diseño";
                break;
         case    4:
                mensaje = "y si hoy aprendemos algo de negocios ";
                break;
         case    5:
                mensaje = "veamos un par de clases de audio visual";
                break;
         case    6:
                mensaje = "talves sea bueno desarrollar una habilidad";
                break;
         case    7:
                mensaje = "yo decido diostaerme programando";
                break;
            default:
                mensaje = "pedir al usuario que ingrese un rango valido.";
                break;
        }

        System.out.println(mensaje);
    }
}
