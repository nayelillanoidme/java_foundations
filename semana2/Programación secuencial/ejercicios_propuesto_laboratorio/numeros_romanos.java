import java.util.Scanner;

public class numeros_romanos{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("un número  : ");
        int numero = scanner.nextInt();
        
        
        int[] valores = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] simbolos = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        
       
        StringBuilder resultado = new StringBuilder();
        
        
        for (int i = 0; i < valores.length; i++) {
            while (numero >= valores[i]) {
                resultado.append(simbolos[i]);
                numero -= valores[i];
            }
        }
        
       
        System.out.println("El número " + numero + " en números romanos es: " + resultado.toString());
    }
}
