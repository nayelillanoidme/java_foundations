/**

* Esta clase define objetos de tipo Persona con un nombre, apellidos,
* número de documento de identidad y año de nacimiento.
*
* @version 1.2/2023
 */
public class Persona {
    String nombre; // Atributo que identifica el nombre de una persona
    String apellidos; // Atributo que identifica los apellidos de una persona
    String paisnacimiento;
    char genero;
    /*
  * Atributo que identifica el número de documento de identidad de
  * una persona
     */
    String númeroDocumentoIdentidad;
    int añoNacimiento; /*
                        *Atributo que identifica el año de nacimiento
                        * de una persona
                        */

    /**
  * Constructor de la clase Persona
  * @param nombre Parámetro que define el nombre de la persona
  * @param apellidos Parámetro que define los apellidos de la persona
  * @param númeroDocumentoIdentidad Parámetro que define el
  * número del documento de identidad de la persona
  * @param añoNacimiento Parámetro que define el año de nacimiento
  * de la persona
    */
    Persona(String nombre, String apellidos, String númeroDocumentoIdentidad, int añoNacimiento, String paísNacimiento,
            char género) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.númeroDocumentoIdentidad = númeroDocumentoIdentidad;
        this.añoNacimiento = añoNacimiento;
        this.paisnacimiento = paísNacimiento;
        this.genero = género;
    }

    void imprimir() {
        System.out.println("nombre: " + nombre);
        System.out.println("apellidos: " + apellidos);
        System.out.println("DNI: " + númeroDocumentoIdentidad);
        System.out.println("año de nacimiento: " + añoNacimiento);
        //System.out.println("pais de nacimiento: " + paisnacimiento);
        //System.out.println("genero: " + genero);
        System.out.println("país de nacimiento: " + paisnacimiento);
        System.out.println("género: " + genero);

        System.out.println();
    }

    public static void main(String[] args) {
        Persona persona1 = new Persona("Jheison", "Alca", "74812963", 2003, "peru", 'H');
        Persona persona2 = new Persona("Nayeli", "Llano", "73512933", 2003, "peru", 'M');
        persona1.imprimir();
        persona2.imprimir();
       



        
       
        
    }
}
