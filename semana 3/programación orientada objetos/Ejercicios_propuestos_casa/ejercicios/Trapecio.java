package ejercicios;

public class Trapecio {
    private double baseMayor;
    private double baseMenor;
    private double ladoNoParalelo1;
    private double ladoNoParalelo2;
    private double altura;

    public Trapecio(double baseMayor, double baseMenor, double ladoNoParalelo1, double ladoNoParalelo2, double altura) {
        this.baseMayor = baseMayor;
        this.baseMenor = baseMenor;
        this.ladoNoParalelo1 = ladoNoParalelo1;
        this.ladoNoParalelo2 = ladoNoParalelo2;
        this.altura = altura;
    }

    public double calcularArea() {
        double area = (baseMayor + baseMenor) * altura / 2;
        return area;
    }

    public double calcularPerimetro() {
        double perimetro = baseMayor + baseMenor + ladoNoParalelo1 + ladoNoParalelo2 /4;
        return perimetro;
    }
}
