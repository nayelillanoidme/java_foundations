package ejercicios;

public class Rombo {
    private double diagonalMayor;
    private double diagonalMenor;

    public Rombo(double diagonalMayor, double diagonalMenor) {
        this.diagonalMayor = diagonalMayor;
        this.diagonalMenor = diagonalMenor;
    }

    public double calcularArea() {
        double area = (diagonalMayor * diagonalMenor) / 2;
        return area;
    }

    public double calcularPerimetro() {
        double lado = Math.sqrt(Math.pow(diagonalMayor / 2, 2) + Math.pow(diagonalMenor / 2, 2));
        double perimetro = lado * 4;
        return perimetro;
    }
}
