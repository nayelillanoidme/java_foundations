public class ordeninverso {
    public static void main(String[] args) {
        imprimirInverso(10); 
    }
    
    public static void imprimirInverso(int n) {
        if (n == 0) { 
            return;
        } else {
            System.out.println(n); 
            imprimirInverso(n - 1); 
        }
    }
}
