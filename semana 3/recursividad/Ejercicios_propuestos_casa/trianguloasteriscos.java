public class trianguloasteriscos {
    
    public static void main(String[] args) {
        dibujarGrafico(7);
    }
    
    public static void dibujarGrafico(int n) {
        if (n <= 0) {
            return;
        }
        
        dibujarGrafico(n - 1, 1);
        System.out.println();
        
        dibujarGrafico(n - 1);
    }
    
    private static void dibujarGrafico(int espacios, int asteriscos) {
        if (asteriscos > espacios) {
            return;
        }
        
        dibujarGrafico(espacios - 1, asteriscos);
        System.out.print(asteriscos == 1 ? "*" : "**");
    }
    
    private static void dibujarGrafico(int n) {
        dibujarGrafico(n, 0);
    }
}
