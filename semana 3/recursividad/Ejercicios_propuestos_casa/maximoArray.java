public class maximoArray {
    public static void main(String[] args) {
        int[] array = {1, 5, 3, 8, 2, 4};
        int maximo = calcularMaximo(array, 0, array.length-1);
        System.out.println("El máximo valor del array es: " + maximo);
    }
    
    public static int calcularMaximo(int[] array, int inicio, int fin) {
        if (inicio == fin) { 
            return array[inicio];
        } else {
            int mitad = (inicio + fin) / 2; 
            int maxIzquierdo = calcularMaximo(array, inicio, mitad); 
            int maxDerecho = calcularMaximo(array, mitad+1, fin); 
            return Math.max(maxIzquierdo, maxDerecho); 
        }
    }
}
