
import java.util.Scanner;
import java.math.BigInteger;

public class Max_comun_divisor {
    
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingrese el primer número: ");
        int numero1 = leer.nextInt();
        System.out.print("Ingrese el segundo número: ");
        int numero2 = leer.nextInt();
        int mcd = calcularMCD(numero1, numero2);
        System.out.println("El Máximo Común Divisor de " + numero1 + " y " + numero2 + " es: " + mcd);
        leer.close();
    }
    
    public static int calcularMCD(int num1, int num2) {
        if (num2 == 0) {
            return num1;
        } else {
            return calcularMCD(num2, num1 % num2);
        }
    }
}