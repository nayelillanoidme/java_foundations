import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class archivotxt{
  
  public static void main(String[] args) {
    ArrayList<Integer> data = generateData();
    
    try {
      File dataFile = new File("datos_generados.txt");
      FileWriter dataWriter = new FileWriter(dataFile);
      for (int i = 0; i < data.size(); i++) {
        dataWriter.write(data.get(i) + "\n");
      }
      dataWriter.close();
      
      int sum = 0;
      int max = data.get(0);
      int min = data.get(0);
      HashMap<Integer, Integer> frequency = new HashMap<>();
      
      for (int i = 0; i < data.size(); i++) {
        int num = data.get(i);
        sum += num;
        if (num > max) {
          max = num;
        }
        if (num < min) {
          min = num;
        }
        if (frequency.containsKey(num)) {
          frequency.put(num, frequency.get(num) + 1);
        } else {
          frequency.put(num, 1);
        }
      }
      
      double average = (double) sum / data.size();
      
      int mode = -1;
      int modeCount = 0;
      for (int num : frequency.keySet()) {
        if (frequency.get(num) > modeCount) {
          modeCount = frequency.get(num);
          mode = num;
        }
      }
      
      double variance = 0;
      for (int i = 0; i < data.size(); i++) {
        double deviation = data.get(i) - average;
        variance += deviation * deviation;
      }
      variance /= data.size();
      double standardDeviation = Math.sqrt(variance);
      
      File resultsFile = new File("resultados.txt");
      FileWriter resultsWriter = new FileWriter(resultsFile);
      resultsWriter.write("Valor máximo: " + max + "\n");
      resultsWriter.write("Valor mínimo: " + min + "\n");
      resultsWriter.write("Promedio: " + average + "\n");
      resultsWriter.write("Moda: " + mode + "\n");
      resultsWriter.write("Desviación estándar: " + standardDeviation + "\n");
      resultsWriter.close();
      
      System.out.println("Datos generados y resultados calculados exitosamente.");
      
    } catch (IOException e) {
      System.out.println("Error al escribir en archivo.");
      e.printStackTrace();
    }
  }
  
  public static ArrayList<Integer> generateData() {
    ArrayList<Integer> data = new ArrayList<>();
    for (int i = 0; i < 1000; i++) {
      int num = (int) (Math.random() * 301);
      data.add(num);
    }
    return data;
  }
}