import java.util.Scanner;
import java.math.BigInteger;

public class min_comun_multiplo {
    public static void main(String[]args) {
        Scanner leer = new Scanner(System.in);
        System.out.println("ingresar el primer numero");
        int numero1 = leer.nextInt();
        System.out.println("ingresar el segundo numero");
        int numero2 = leer.nextInt();
        int mcm = calcularMCM(numero1, numero2);
        System.out.println("El Mínimo Común Múltiplo de " + numero1 + " y " + numero2 + " es: " + mcm);
        leer.close();
    }
    
    public static int calcularMCM(int num1, int num2) {
        int mcm = (num1 * num2) / calcularMCM(num1, num2);
        return mcm;

        
    }
    

}
