
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class permutaciones{
    
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de elementos: ");
        int n = leer.nextInt();
        List<Integer> elementos = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el elemento " + (i + 1) + ": ");
            elementos.add(leer.nextInt());
        }
        int numPermutaciones = calcularPermutaciones(elementos.size());
        System.out.println("El número de permutaciones de los " + n + " elementos es: " + numPermutaciones);
        leer.close();
    }
    
    public static int calcularPermutaciones(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * calcularPermutaciones(n - 1);
        }
    }
}


