import java.util.Scanner;

public class numerospares {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese una lista de números separados por comas: ");
        String lista = sc.nextLine();

        String[] numeros = lista.split(",");
        int pares = 0;

        for (String numero : numeros) {
            if (Integer.parseInt(numero.trim()) % 2 == 0) {
                pares++;
            }
        }

        System.out.println("Hay " + pares + " números pares en la lista.");
    }
}
