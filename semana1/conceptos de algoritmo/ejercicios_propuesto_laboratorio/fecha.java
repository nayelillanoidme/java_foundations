import java.util.Scanner;

public class fecha {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese una fecha en formato dd/mm/yyyy: ");
        String fecha = sc.nextLine();

        boolean fechaValida = true;

        try {
            String[] partesFecha = fecha.split("/");
            int dia = Integer.parseInt(partesFecha[0]);
            int mes = Integer.parseInt(partesFecha[1]);
            int anio = Integer.parseInt(partesFecha[2]);

            int[] diasPorMes = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

            if (anio % 4 == 0 && (anio % 100 != 0 || anio % 400 == 0)) {
                diasPorMes[2] = 29;
            }

            if (mes < 1 || mes > 12) {
                fechaValida = false;
            } else if (dia < 1 || dia > diasPorMes[mes]) {
                fechaValida = false;
            }
        } catch (Exception e) {
            fechaValida = false;
        }

        if (fechaValida) {
            System.out.println("La fecha es válida.");
        } else {
            System.out.println("La fecha no es válida.");
        }
    }
}
