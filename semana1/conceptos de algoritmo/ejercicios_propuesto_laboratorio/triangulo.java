import java.util.Scanner;

public class triangulo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el lado 1 del triángulo: ");
        int lado1 = sc.nextInt();
        System.out.println("Ingrese el lado 2 del triángulo: ");
        int lado2 = sc.nextInt();
        System.out.println("Ingrese el lado 3 del triángulo: ");
        int lado3 = sc.nextInt();
        if (lado1 + lado2 > lado3 && lado2 + lado3 > lado1 && lado1 + lado3 > lado2) {
            System.out.println("Los tres números pueden formar un triángulo válido.");
        } else {
            System.out.println("Los tres números no pueden formar un triángulo válido.");
        }
        sc.close();
    }
}
