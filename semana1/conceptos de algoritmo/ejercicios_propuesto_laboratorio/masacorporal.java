import java.util.Scanner;

public class masacorporal {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese su peso en kg: ");
        float peso = sc.nextFloat();

        System.out.print("Ingrese su estatura en metros: ");
        float estatura = sc.nextFloat();

        float imc = peso / (estatura * estatura);

        System.out.println("Tu IMC es: " + imc);

        if (imc < 18.5) {
            System.out.println("Tienes bajo peso.");
        } else if (imc < 25) {
            System.out.println("Tu peso es normal.");
        } else if (imc < 30) {
            System.out.println("Tienes sobrepeso.");
        } else {
            System.out.println("Tienes obesidad.");
        }
    }
}
