public class fibonacci {
  public static void main(String[] args) {
    int max = 10000;
    int numPerfectos = 0;
    int numFibonacci = 0;
    for (int i = 1; i <= max; i++) {

      int sumaDivisores = 0;
      for (int j = 1; j < i; j++) {
        if (i % j == 0) {
          sumaDivisores += j;
        }
      }
      if (sumaDivisores == i) {
        numPerfectos++;
      }
      int a = 0;
      int b = 1;
      int c = 1;
      while (c < i) {
        a = b;
        b = c;
        c = a + b;
      }
      if (c == i) {
        numFibonacci++;
      }
    }

    System.out.println("Hay " + numPerfectos + " números perfectos y " + numFibonacci + " números de Fibonacci.");
  }
}
