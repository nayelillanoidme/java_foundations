import java.util.Scanner;

public class vocales {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese una lista de palabras separadas por comas: ");
        String lista = sc.nextLine();

        String[] palabras = lista.split(",");
        int cont = 0;

        for (String palabra : palabras) {
            boolean consecutivas = false;
            int vocalesConsecutivas = 0;
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                if ("aeiouAEIOU".indexOf(letra) != -1) {
                    vocalesConsecutivas++;
                    if (vocalesConsecutivas > 2) {
                        consecutivas = true;
                        break;
                    }
                } else {
                    vocalesConsecutivas = 0;
                }
            }
            if (consecutivas) {
                cont++;
            }
        }

        System.out.println("Hay " + cont + " palabras con más de dos vocales consecutivas.");
    }
}
