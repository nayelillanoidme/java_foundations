public class Ndivisible {
  public static void main(String[] args) {
    int max = 1000000;
    int cantidadDivisibles = 0;
    for (int i = 1; i <= max; i++) {
      boolean esDivisible = true;
      for (int j = 1; j <= 10; j++) {
        if (i % j != 0) {
          esDivisible = false;
          break;
        }
      }
      if (esDivisible) {
        cantidadDivisibles++;
      }
    }
    System.out.println("Hay " + cantidadDivisibles + " números divisibles por todos los números de 1 a 10.");
  }
}
