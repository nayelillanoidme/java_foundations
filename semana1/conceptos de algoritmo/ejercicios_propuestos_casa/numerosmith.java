public class numerosmith {
  public static void main(String[] args) {
    int limite = 1000000;
    int cantidadDeSmith = 0;
    for (int i = 1; i <= limite; i++) {
      if (esDeSmith(i)) {
        cantidadDeSmith++;
      }
    }
    System.out.println("Hay " + cantidadDeSmith + " números de Smith hasta " + limite + ".");
  }
  
  public static boolean esDeSmith(int n) {
    int sumaDigitos = sumaDigitos(n);
    int sumaDigitosFactoresPrimos = 0;
    int factor = 2;
    while (n > 1) {
      if (n % factor == 0) {
        sumaDigitosFactoresPrimos += sumaDigitos(factor);
        n /= factor;
      } else {
        factor++;
      }
    }
    return sumaDigitos == sumaDigitosFactoresPrimos && sumaDigitos != 1;
  }
  
  public static int sumaDigitos(int n) {
    int suma = 0;
    while (n > 0) {
      suma += n % 10;
      n /= 10;
    }
    return suma;
  }
}
