public class numerosenteros {
  public static void main(String[] args) {
    int limite = 1000000;
    int cantidadConPropiedad = 0;
    for (int i = 1; i <= limite; i++) {
      int sumaFactoriales = 0;
      int numero = i;
      while (numero > 0) {
        int digito = numero % 10;
        sumaFactoriales += factorial(digito);
        numero /= 10;
      }
      if (sumaFactoriales == i) {
        cantidadConPropiedad++;
      }
    }
    System.out.println("Hay " + cantidadConPropiedad + " números que tienen la propiedad de ser iguales a la suma de los dígitos factoriales de su descomposición en dígitos.");
  }
  
  public static int factorial(int n) {
    int resultado = 1;
    for (int i = 1; i <= n; i++) {
      resultado *= i;
    }
    return resultado;
  }
}
