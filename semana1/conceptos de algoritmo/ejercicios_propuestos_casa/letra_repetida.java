import java.util.HashMap;

public class letra_repetida {
  public static void main(String[] args) {
    String[] palabras = {"perro", "gato", "oso", "murcielago", "casa", "espectaculo", "amoroso", "aeropuerto"};
    int cantidadConLetrasRepetidasImpar = 0;
    for (String palabra : palabras) {
      HashMap<Character, Integer> letras = new HashMap<Character, Integer>();
      for (int i = 0; i < palabra.length(); i++) {
        char letra = palabra.charAt(i);
        if (letras.containsKey(letra)) {
          letras.put(letra, letras.get(letra) + 1);
        } else {
          letras.put(letra, 1);
        }
      }
      boolean tieneLetraRepetidaImpar = false;
      for (int cantidad : letras.values()) {
        if (cantidad % 2 != 0) {
          tieneLetraRepetidaImpar = true;
          break;
        }
      }
      if (tieneLetraRepetidaImpar) {
        cantidadConLetrasRepetidasImpar++;
      }
    }
    System.out.println("Hay " + cantidadConLetrasRepetidasImpar + " palabras que tienen una letra repetida un número impar de veces.");
  }
}
