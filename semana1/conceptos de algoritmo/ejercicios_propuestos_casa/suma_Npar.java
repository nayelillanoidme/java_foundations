import java.util.Scanner;

public class suma_Npar {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese una lista de números separados por comas: ");
        String lista = sc.nextLine();

        System.out.print("Ingrese un número: ");
        int objetivo = sc.nextInt();

        String[] numeros = lista.split(",");
        boolean encontrado = false;

        for (int i = 0; i < numeros.length - 1; i++) {
            for (int j = i + 1; j < numeros.length; j++) {
                int num1 = Integer.parseInt(numeros[i].trim());
                int num2 = Integer.parseInt(numeros[j].trim());
                if (num1 + num2 == objetivo) {
                    System.out.println("El par " + num1 + " y " + num2 + " suman " + objetivo + ".");
                    encontrado = true;
                    break;
                }
            }
            if (encontrado) {
                break;
            }
        }

        if (!encontrado) {
            System.out.println("No se encontró ningún par de números que sumen " + objetivo + ".");
        }
    }
}
