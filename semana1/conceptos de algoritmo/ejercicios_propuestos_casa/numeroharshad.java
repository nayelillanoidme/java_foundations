public class numeroharshad {
  public static void main(String[] args) {
    int limite = 1000000;
    int cantidadDeHarshad = 0;
    for (int i = 1; i <= limite; i++) {
      if (esDeHarshad(i)) {
        cantidadDeHarshad++;
      }
    }
    System.out.println("Hay " + cantidadDeHarshad + " números de Harshad hasta " + limite + ".");
  }
  
  public static boolean esDeHarshad(int n) {
    int sumaDigitos = sumaDigitos(n);
    return n % sumaDigitos == 0;
  }
  
  public static int sumaDigitos(int n) {
    int suma = 0;
    while (n > 0) {
      suma += n % 10;
      n /= 10;
    }
    return suma;
  }
}
