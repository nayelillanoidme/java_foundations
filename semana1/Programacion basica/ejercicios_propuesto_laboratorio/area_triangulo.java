import java.util.Scanner;

public class area_triangulo {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double base, altura, area;

    System.out.print("Ingresa la base del triángulo: ");
    base = sc.nextDouble();

    System.out.print("Ingresa la altura del triángulo: ");
    altura = sc.nextDouble();

    area = (base * altura) / 2;

    System.out.println("El área del triángulo es: " + area);

    sc.close();
  }
}
