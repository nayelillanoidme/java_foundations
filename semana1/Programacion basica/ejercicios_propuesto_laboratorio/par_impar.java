import java.util.Scanner;

public class par_impar {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int num;

    System.out.print("Ingresa un número entero: ");
    num = sc.nextInt();

    if (num % 2 == 0) {
      System.out.println(num + " es un número par.");
    } else {
      System.out.println(num + " es un número impar.");
    }

    sc.close();
  }
}
