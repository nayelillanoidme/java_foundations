import java.util.Scanner;

public class cociente_resto {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int num1, num2, cociente, resto;

    System.out.print("Ingresa el primer número: ");
    num1 = sc.nextInt();

    System.out.print("Ingresa el segundo número: ");
    num2 = sc.nextInt();

    cociente = num1 / num2;
    resto = num1 % num2;

    System.out.println("El cociente es: " + cociente);
    System.out.println("El resto es: " + resto);

    sc.close();
  }
}
