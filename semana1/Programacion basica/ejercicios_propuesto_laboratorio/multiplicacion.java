import java.util.Scanner;

public class multiplicacion {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingresa el primer número: ");
        int num1 = input.nextInt();
        System.out.print("Ingresa el segundo número: ");
        int num2 = input.nextInt();
        int producto = num1 * num2;
        System.out.println("El producto de los números es: " + producto);
    }
}
