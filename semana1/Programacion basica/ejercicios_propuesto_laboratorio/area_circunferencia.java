import java.util.Scanner;

public class circunferencia{
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double radio, area, circunferencia;

    System.out.print("Ingresa el radio del círculo: ");
    radio = sc.nextDouble();

    area = Math.PI * radio * radio;
    circunferencia = 2 * Math.PI * radio;

    System.out.println("El área del círculo es: " + area);
    System.out.println("La circunferencia del círculo es: " + circunferencia);

    sc.close();
  }
}
