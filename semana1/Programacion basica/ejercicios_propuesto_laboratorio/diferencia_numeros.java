import java.util.Scanner;

public class diferencia_numeros {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingresa el primer número: ");
        int num1 = input.nextInt();
        System.out.print("Ingresa el segundo número: ");
        int num2 = input.nextInt();
        int diferencia = num1 - num2;
        System.out.println("La diferencia de los números es: " + diferencia);
    }
}
