import java.util.Scanner;

public class scanner {
  public static void main(String[] args) {
    
    Scanner sc = new Scanner(System.in);

    System.out.println("Ingrese una cadena de texto: ");
    String texto = sc.nextLine();

    System.out.println("La cadena de texto ingresada es: " + texto);
    System.out.println("Ingrese un número entero: ");
    int numero = sc.nextInt();

    System.out.println("El número entero ingresado es: " + numero);

    
    sc.close();
  }
}
