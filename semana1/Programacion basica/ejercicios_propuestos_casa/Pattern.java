import java.util.regex.Pattern;

public class Pattern {
    public static void main(String[] args) {
        
        String expresionRegular = "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}";

        
        Pattern pattern = Pattern.compile(expresionRegular);
        String texto = "Hola, mi correo es ejemplo@gmail.com, gracias";

        Matcher matcher = pattern.matcher(texto);


        while (matcher.find()) {
            System.out.println("Coincidencia encontrada: " + matcher.group());
        }
    }
}
