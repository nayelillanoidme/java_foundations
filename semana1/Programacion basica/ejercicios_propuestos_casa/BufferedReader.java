import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BufferedReader {
  public static void main(String[] args) {
    
    String rutaArchivo = "ruta/del/archivo.txt";

    try {
      
      FileReader fr = new FileReader(rutaArchivo);
      BufferedReader br = new BufferedReader(fr);

      String linea;
      while ((linea = br.readLine()) != null) {
        System.out.println(linea);
      }

      br.close();
      fr.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}


