import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Marcher {
    public static void main(String[] args) {
        String regex = "Java";
        String text = "Java es un lenguaje de programación muy popular";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(text);

        if (matcher.find()) {
            System.out.println("Se encontró el patrón en la posición " + matcher.start());
        } else {
            System.out.println("No se encontró el patrón");
        }
    }
}
