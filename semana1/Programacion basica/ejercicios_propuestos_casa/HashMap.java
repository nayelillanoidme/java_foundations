
HashMap<String, Integer> edades = new HashMap<>();

edades.put("Juan", 30);
edades.put("María", 25);
edades.put("Pedro", 40);
edades.put("Lucía", 35);
edades.put("Carlos", 20);

int edadDeJuan = edades.get("Juan");

for (String nombre : edades.keySet()) {
    int edad = edades.get(nombre);
    System.out.println(nombre + " tiene " + edad + " años");
}
