import java.util.Calendar;

public class calendar {
  public static void main(String[] args) {
    
    Calendar fechaActual = Calendar.getInstance();

    int año = fechaActual.get(Calendar.YEAR);
    int mes = fechaActual.get(Calendar.MONTH) + 1; // Los meses comienzan en 0
    int dia = fechaActual.get(Calendar.DAY_OF_MONTH);

    
    System.out.println("Fecha actual: " + dia + "/" + mes + "/" + año);
  }
}
