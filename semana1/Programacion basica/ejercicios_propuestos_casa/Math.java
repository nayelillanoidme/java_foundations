public class Math {
  public static void main(String[] args) {
    double a = 25.0;
    double b = 4.0;
    
    double raizCuadrada = Math.sqrt(a);
    System.out.println("La raíz cuadrada de " + a + " es " + raizCuadrada);
    
    
    double valorAbsoluto = Math.abs(b);
    System.out.println("El valor absoluto de " + b + " es " + valorAbsoluto);
    
    
    double valorMaximo = Math.max(a, b);
    System.out.println("El valor máximo entre " + a + " y " + b + " es " + valorMaximo);
    
    double valorMinimo = Math.min(a, b);
    System.out.println("El valor mínimo entre " + a + " y " + b + " es " + valorMinimo);
    double resultadoPotencia = Math.pow(a, b);
    System.out.println(a + " elevado a la potencia de " + b + " es igual a " + resultadoPotencia);
    

    double valorRedondeado = Math.round(a);
    System.out.println("El valor redondeado de " + a + " es " + valorRedondeado);
  }
}
