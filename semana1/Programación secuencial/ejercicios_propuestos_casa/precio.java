import java.util.Scanner;

public class precio {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el precio de costo del producto: ");
        double precioCosto = sc.nextDouble();
        System.out.print("Ingrese el porcentaje de ganancia: ");
        double porcentajeGanancia = sc.nextDouble();
        System.out.print("Ingrese el porcentaje de impuestos: ");
        double porcentajeImpuestos = sc.nextDouble();
        sc.close();

    
        double precioVenta = precioCosto * (1 + porcentajeGanancia / 100) * (1 + porcentajeImpuestos / 100);

        System.out.println("El precio de venta final es: " + precioVenta);
    }
}
