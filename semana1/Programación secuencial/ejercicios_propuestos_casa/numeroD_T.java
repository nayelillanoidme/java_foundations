import java.util.Scanner;

public class numeroD_T {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese un número entero: ");
        int num = sc.nextInt();

        int doble = num * 2;
        int triple = num * 3;

        System.out.println("El doble de " + num + " es " + doble);
        System.out.println("El triple de " + num + " es " + triple);
    }
}
