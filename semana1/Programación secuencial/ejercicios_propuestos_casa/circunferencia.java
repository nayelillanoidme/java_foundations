import java.util.Scanner;

public class circunferencia {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese el valor del radio de la circunferencia: ");
        double radio = sc.nextDouble();

        double longitud = 2 * Math.PI * radio;
        double area = Math.PI * radio * radio;

        System.out.println("La longitud de la circunferencia es " + longitud);
        System.out.println("El área de la circunferencia es " + area);
    }
}
