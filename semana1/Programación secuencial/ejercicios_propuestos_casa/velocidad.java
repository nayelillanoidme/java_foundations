import java.util.Scanner;

public class velocidad {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double kmh, ms;
        System.out.print("Introduce la velocidad en Km/h: ");
        kmh = sc.nextDouble();
        ms = kmh * 0.27777777777778; // 1 km/h = 0.27777777777778 m/s
        System.out.println(kmh + " Km/h equivalen a " + ms + " m/s.");
    }
}
