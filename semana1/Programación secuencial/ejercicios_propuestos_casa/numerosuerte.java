import java.util.Scanner;

public class numerosuerte {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese su fecha de nacimiento (dd/mm/yyyy): ");
        String fecha = sc.nextLine();
        sc.close();

        String[] fechaSplit = fecha.split("/");
        int dia = Integer.parseInt(fechaSplit[0]);
        int mes = Integer.parseInt(fechaSplit[1]);
        int anio = Integer.parseInt(fechaSplit[2]);

      
        int sumaDigitos = sumarDigitos(dia) + sumarDigitos(mes) + sumarDigitos(anio);

        
        int numeroSuerte = 0;
        while (sumaDigitos > 0) {
            numeroSuerte += sumaDigitos % 10;
            sumaDigitos /= 10;
        }

        System.out.println("Tu número de la suerte es: " + numeroSuerte);
    }

  
    public static int sumarDigitos(int num) {
        int suma = 0;
        while (num > 0) {
            suma += num % 10;
            num /= 10;
        }
        return suma;
    }
}
