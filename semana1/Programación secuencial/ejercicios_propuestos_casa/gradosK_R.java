import java.util.Scanner;

public class gradosK_R {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double gradosCelsius, gradosKelvin, gradosReamur;

        System.out.print("Ingrese la temperatura en grados Celsius: ");
        gradosCelsius = sc.nextDouble();

        
        gradosKelvin = gradosCelsius + 273.15;

        gradosReamur = gradosCelsius * 0.8;

        System.out.println("Grados Celsius: " + gradosCelsius);
        System.out.println("Grados Kelvin: " + gradosKelvin);
        System.out.println("Grados Reamur: " + gradosReamur);
    }
}
