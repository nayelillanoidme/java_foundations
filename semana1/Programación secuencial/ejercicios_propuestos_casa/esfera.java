import java.util.Scanner;

public class esfera {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double radio, volumen;
        System.out.print("Introduce el radio de la esfera: ");
        radio = sc.nextDouble();
        volumen = (4.0 / 3.0) * Math.PI * Math.pow(radio, 3);
        System.out.println("El volumen de la esfera es: " + volumen);
    }
}
