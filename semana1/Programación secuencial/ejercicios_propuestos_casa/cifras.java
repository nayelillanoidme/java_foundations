import java.util.Scanner;

public class cifras {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = sc.nextInt();
        System.out.print("Ingrese la cantidad de cifras a quitar: ");
        int cifras = sc.nextInt();
        sc.close();

        int resultado = num / (int) Math.pow(10, cifras);

        System.out.println("El resultado es: " + resultado);
    }
}
