import java.util.Scanner;

public class temperatura {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese una cantidad de grados centígrados: ");
        double celsius = sc.nextDouble();

        double fahrenheit = 32 + (9 * celsius / 5);

        System.out.println(celsius + " grados centígrados equivalen a " + fahrenheit + " grados Fahrenheit");
    }
}
