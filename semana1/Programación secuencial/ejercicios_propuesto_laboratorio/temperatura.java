import java.util.Scanner;

public class temperatura {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.print("Ingrese una temperatura en grados Celsius: ");
    double celsius = sc.nextDouble();
    double fahrenheit = (9 * celsius / 5) + 32;
    System.out.println(celsius + " grados Celsius equivalen a " + fahrenheit + " grados Fahrenheit.");
  }
}
